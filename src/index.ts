import Module from './interfaces/Module';
import Command from './interfaces/Command';
import {Client, Message, MessageEmbed} from 'discord.js';

/**
 *
 *
 * @class ExampleCommand
 * @implements {Module}
 */
class ExampleCommand implements Command {
    public name: string
    public description: string
    public version: string
    public author: string
    public module: string
    public aliases: string[]
    public execute: (client: Client, message: Message, args: string[]) => void
    /**
 * Creates an instance of ExampleCommand.
 * @memberof ExampleCommand
 */
    constructor() {
      this.name = 'Example';
      this.description = 'an example module';
      this.version = '0.0.1';
      this.author = 'Shiroraven <me@shiro.dev>';
      this.execute = (client, message) => {
        const e = new MessageEmbed();
        e.setTitle('abc');
        e.addField('field', 'somevalue');
        message.channel.send(e);
      };
    }
}
/**
 *
 *
 * @class ExampleModule
 * @implements {Module}
 */
class ExampleModule implements Module {
    public name: string
    public description: string
    public version: string
    public author: string
    public commands: Command[]
    /**
 * Creates an instance of ExampleModule.
 * @memberof ExampleModule
 */
    constructor() {
      this.name = 'Example';
      this.description = 'an example module';
      this.version = '0.0.1';
      this.author = 'Shiroraven <me@shiro.dev>';
      this.commands = [new ExampleCommand()];
    }
}
module.exports = new ExampleModule();

