import {Client, Message} from 'discord.js';

export default interface Command {
  module: string,
  name: string,
  author?: string,
  description?: string,
  aliases: string[],
  execute: (client: Client, message?: Message, args?: string[]) => void
// eslint-disable-next-line semi
}
