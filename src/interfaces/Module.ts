import Command from '@interfaces/Command';

export default interface Module {
  name: string,
  author?: string,
  description?: string,
  version: string,
  commands: Command[]
// eslint-disable-next-line semi
}
